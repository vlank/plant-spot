﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApp1.Components;

public partial class ResponsiveBlockContent : UserControl
{
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
        nameof(Text), typeof(string), typeof(ResponsiveBlockContent), new PropertyMetadata(default(string)));

    public string Text
    {
        get { return (string)GetValue(TextProperty); }
        set { SetValue(TextProperty, value); }
    }
    public ResponsiveBlockContent()
    {
        InitializeComponent();
    }
}