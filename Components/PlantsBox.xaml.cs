﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfApp1.Components;

public partial class PlantsBox : UserControl
{
    public static readonly DependencyProperty InputBoxProperty = DependencyProperty.Register(
        nameof(InputBox), typeof(int), typeof(PlantsBox), new PropertyMetadata(default(int)));

    public int InputBox
    {
        get { return (int)GetValue(InputBoxProperty); }
        set { SetValue(InputBoxProperty, value); }
    }
    public static readonly DependencyProperty TitleBoxProperty = DependencyProperty.Register(
        nameof(TitleBox), typeof(string), typeof(PlantsBox), new PropertyMetadata(default(string)));

    public string TitleBox
    {
        get { return (string)GetValue(TitleBoxProperty); }
        set { SetValue(TitleBoxProperty, value); }
    }
    public PlantsBox()
    {
        InitializeComponent();
    }
    private void CheckOnlyNumbersInput(object sender, TextCompositionEventArgs e)
    {
        Regex regex = new Regex("[^0-9]+");
        e.Handled = regex.IsMatch(e.Text);
    }
}