﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApp1.Components;

public partial class ResponsiveBlock : UserControl
{
    public static readonly DependencyProperty ContentProperty = DependencyProperty.Register(
        nameof(Content), typeof(string), typeof(ResponsiveBlock), new PropertyMetadata(default(string)));

    public string Content
    {
        get { return (string)GetValue(ContentProperty); }
        set { SetValue(ContentProperty, value); }
    }
    public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
        nameof(Title), typeof(string), typeof(ResponsiveBlock), new PropertyMetadata(default(string)));

    public string Title
    {
        get { return (string)GetValue(TitleProperty); }
        set { SetValue(TitleProperty, value); }
    }
    public ResponsiveBlock()
    {
        InitializeComponent();
    }
}