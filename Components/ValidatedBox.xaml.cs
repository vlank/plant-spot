﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApp1.Components;

public partial class ValidatedBox : UserControl
{
    public static readonly DependencyProperty MaxLengthProperty = DependencyProperty.Register(
        nameof(MaxLength), typeof(string), typeof(ValidatedBox), new PropertyMetadata(default(string)));

    public string MaxLength
    {
        get { return (string)GetValue(MaxLengthProperty); }
        set { SetValue(MaxLengthProperty, value); }
    }
    public static readonly DependencyProperty TitleBlockProperty = DependencyProperty.Register(
        nameof(TitleBlock), typeof(string), typeof(ValidatedBox), new PropertyMetadata(default(string)));

    public string TitleBlock
    {
        get { return (string)GetValue(TitleBlockProperty); }
        set { SetValue(TitleBlockProperty, value); }
    }
    public static readonly DependencyProperty ValidateTextProperty = DependencyProperty.Register(
        nameof(ValidateText), typeof(object), typeof(ValidatedBox), new PropertyMetadata(default(object)));

    public object ValidateText
    {
        get { return (object)GetValue(ValidateTextProperty); }
        set { SetValue(ValidateTextProperty, value); }
    }
    public static readonly DependencyProperty TextContentProperty = DependencyProperty.Register(
        nameof(TextContent), typeof(string), typeof(ValidatedBox), new PropertyMetadata(default(string)));

    public string TextContent
    {
        get { return (string)GetValue(TextContentProperty); }
        set { SetValue(TextContentProperty, value); }
    }
    public ValidatedBox()
    {
        InitializeComponent();
    }
}