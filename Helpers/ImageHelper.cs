﻿using System.IO;
using System.Windows;
using Microsoft.Win32;

namespace WpfApp1.Helpers;

public static class ImageHelper
{
    public static string ImageSource { get; set; } = string.Empty;

    public static string? LoadImage()
    {
        OpenFileDialog openDialog = new()
        {
            Filter = "Image files | *.png; *jpg",
            FilterIndex = 1
        };
        if (openDialog.ShowDialog() is true)
        {
            var directory = EnsureCreatedImagesPath();
            var imagePath = openDialog.FileName;
            var fileName = Path.GetFileName(imagePath);
            var existingFilePath = Path.Combine(directory, fileName);

            if (File.Exists(existingFilePath))
            {
                ImageSource = existingFilePath;
                MessageBox.Show("Se ha seleccionado una imagen preexistente");
            }
            else
            {
                ImageSource = CopyImage(imagePath, directory);
            }
        }
        
        return ImageSource;
    }

    public static void DeleteAllImages()
    {
        try
        {
            var directory = Environment.CurrentDirectory + @"\Assets\Images";
            if (Directory.Exists(directory))
            {
                foreach (var filePath in Directory.EnumerateFiles(directory))
                {
                    File.Delete(filePath);
                }
            }
        }
        catch (IOException e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    private static string EnsureCreatedImagesPath()
    {
        var directory = Environment.CurrentDirectory + @"\Assets\Images";
        if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
        return directory;
    }
    private static string CopyImage(string imagePath, string directory)
    {
        try
        {
            var path = Path.Combine(directory, Path.GetFileName(imagePath));
            File.Copy(imagePath, path, true);
            ImageSource = path;
            MessageBox.Show("Imagen guardada");
        }
        catch (Exception e)
        {
            MessageBox.Show(e.Message);
        }

        return ImageSource;
    }
}