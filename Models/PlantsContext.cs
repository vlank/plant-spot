﻿using Microsoft.EntityFrameworkCore;

namespace WpfApp1.Models
{
    public class PlantsContext : DbContext
    {
        public DbSet<Plant> Plants { get; set; }
        public DbSet<Category> Categories { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder options) {
            options.UseSqlite("DataSource=plantings.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new() { Id = 1 , Name = "Exterior" },
                new() { Id = 2 , Name = "Interior" }
                );
        }
    }

}
