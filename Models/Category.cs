﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace WpfApp1.Models;
[Index(nameof(Name), IsUnique = true)]

public class Category
{
    [Key]
    public int Id { get; init; }
    [Column(TypeName = "varchar(50)"), MaxLength(50)]
    public string Name { get; set; }
    
    public ICollection<Plant> Plants { get; }
}