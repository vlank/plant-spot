﻿namespace WpfApp1.Models;

public class PlantDto(
    int id,
    string name,
    Category category,
    string? description,
    string location,
    string? imageSource,
    DateOnly? plantingDate,
    int? waterTime,
    int? sprayTime,
    int? rotateTime,
    int? pruneTime,
    int? fertilizeTime,
    int? repotTime)
{
    public int Id { get; init; } = id;
    public string Name { get; set; } = name;
    public Category Category { get; set; } = category;
    public string? Description { get; set; } = description;
    public string Location { get; set; } = location;
    public string? ImageSource { get; set; } = imageSource;
    public DateOnly? PlantingDate { get; set; } = plantingDate;
    public int? WaterTime { get; set; } = waterTime;
    public int? SprayTime { get; set; } = sprayTime;
    public int? RotateTime { get; set; } = rotateTime;
    public int? PruneTime { get; set; } = pruneTime;
    public int? FertilizeTime { get; set; } = fertilizeTime;
    public int? RepotTime { get; set; } = repotTime;
}