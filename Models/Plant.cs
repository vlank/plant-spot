﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WpfApp1.Models;

public class Plant
{
    [Key]
    public int Id { get; init; }
    [Column(TypeName = "varchar(30)"), MaxLength(30)] public string Name { get; set; }
    
    public int CategoryId { get; set; }

    [Column(TypeName = "varchar(100)"), MaxLength(100)] public string? Description { get; set; }
    [Column(TypeName = "varchar(40)"), MaxLength(40)] public string Location { get; set; }
    [Column(TypeName = "varchar(200)"), MaxLength(200)] public string? ImageSource { get; set; }
    [Column(TypeName = "date")] public DateOnly? PlantingDate { get; set; }

    [Column(TypeName = "int(2)"), MaxLength(2)] public int? WaterTime { get; set; }
    [Column(TypeName = "int(2)"), MaxLength(2)] public int? SprayTime { get; set; }
    [Column(TypeName = "int(2)"), MaxLength(2)] public int? RotateTime { get; set; }
    [Column(TypeName = "int(2)"), MaxLength(2)] public int? PruneTime { get; set; }
    [Column(TypeName = "int(2)"), MaxLength(2)] public int? FertilizeTime { get; set; }
    [Column(TypeName = "int(2)"), MaxLength(2)] public int? RepotTime { get; set; }
    
    public Category Category { get; set; }
}