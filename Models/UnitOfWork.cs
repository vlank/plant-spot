﻿using Microsoft.EntityFrameworkCore;
using WpfApp1.Interfaces;
using WpfApp1.Repositories;

namespace WpfApp1.Models;

public class UnitOfWork : IUnitOfWork
{
    private readonly DbContext _context;

    public UnitOfWork(DbContext context)
    {
        _context = context;
        Plants = new PlantsRepository(_context);
        Categories = new CategoriesRepository(_context);
    }
    public IPlantsRepository Plants { get; private set; }
    public ICategoryRepository Categories { get; private set; }

    public int Save()
    {
        return _context.SaveChanges();
    }

    public void Dispose()
    {
        _context.Dispose();
    }
    
}