﻿using WpfApp1.Repositories;

namespace WpfApp1.Interfaces;

public interface IUnitOfWork : IDisposable
{
    IPlantsRepository Plants { get; }
    ICategoryRepository Categories { get; }
    int Save();
}