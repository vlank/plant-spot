﻿using WpfApp1.Models;

namespace WpfApp1.Repositories;

public interface IPlantsRepository : IRepository<Plant>
{
    IEnumerable<Plant> GetOrderedPlants(int count);
    IEnumerable<Plant> GetPlantsByCategory(string category);
}