﻿using WpfApp1.Models;
using WpfApp1.Repositories;

namespace WpfApp1.Interfaces;

public interface ICategoryRepository : IRepository<Category>
{
    
}