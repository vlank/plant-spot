﻿# Información técnica
El proyecto consiste en una aplicación de escritorio 
desarrollada sobre Windows Presentation Foundation, mediante
la cual se accede a una base de datos local (SQLite) interactuando
con EntityFramework utilizando el enfoque Domain-Driven priorizando
el desarrollo del dominio para representar la lógica del negocio.
Se utilizan las herramientas del Community Toolkit MVVM para agilizar
la exposición de comandos y notificación de cambios de atributos 
que comunican a la interfaz para permitir la interacción del
usuario.
## Patrones de diseño
### MVVM
La misma utiliza un patrón arquitectónico que busca abstraer la 
interfaz de usuario (View), de la lógica de la aplicación (ViewModel) y el
dominio (Model) que representa nuestra aplicación empresarial.

Esto se realiza para separar las actividades que le corresponden a cada
parte del programa, obteniendo una aplicación fácilmente testeable,
sea tanto para evaluar el desempeño de la interfaz como para comprobar
si la lógica aplicada es correcta para el negocio. Además, en el caso
de abordar un proyecto donde colaboren varias personas, se puede
delegar el desarrollo de una o varias partes a las áreas correspondientes.
Por ejemplo: el desarrollo de la interfaz entre diseñadores y maquetadores,
el tratamiento del dominio que representa la lógica del negocio, etc.
### Dependency Injection
Se utiliza para satisfacer las dependencias reiteradas de ciertos
objetos utilizados a lo largo de toda la aplicación, que en caso
de ser creados en cada caso de uso, implican un aumento en la
memoria utilizada ya que cada objeto consume una cantidad preestablecida
para almacenar sus datos.
Sin embargo, por sobrepasar mi nivel de conocimiento actual,
esto fue manejado hasta el punto más alto de la jerarquía que
puede suministrar los objetos a la aplicación, creando la
instancia en su constructor.