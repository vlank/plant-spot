﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WpfApp1.Migrations
{
    /// <inheritdoc />
    public partial class AddNullability : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "WaterTime",
                table: "Plants",
                type: "int(2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "SprayTime",
                table: "Plants",
                type: "int(2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "RotateTime",
                table: "Plants",
                type: "int(2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "RepotTime",
                table: "Plants",
                type: "int(2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "PruneTime",
                table: "Plants",
                type: "int(2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "FertilizeTime",
                table: "Plants",
                type: "int(2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "CleanTime",
                table: "Plants",
                type: "int(2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(2)");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "WaterTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SprayTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RotateTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RepotTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PruneTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FertilizeTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CleanTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(2)",
                oldNullable: true);
        }
    }
}
