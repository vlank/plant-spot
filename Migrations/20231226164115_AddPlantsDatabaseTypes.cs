﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WpfApp1.Migrations
{
    /// <inheritdoc />
    public partial class AddPlantsDatabaseTypes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plants_Plants_PlantId1",
                table: "Plants");

            migrationBuilder.DropIndex(
                name: "IX_Plants_PlantId1",
                table: "Plants");

            migrationBuilder.DropColumn(
                name: "PlantId1",
                table: "Plants");

            migrationBuilder.AlterColumn<int>(
                name: "WaterTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "SprayTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "RotateTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "RepotTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "PruneTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<DateOnly>(
                name: "PlantingDate",
                table: "Plants",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateOnly),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<string>(
                name: "Location",
                table: "Plants",
                type: "varchar(50)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "FertilizeTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<int>(
                name: "CleanTime",
                table: "Plants",
                type: "int(2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "WaterTime",
                table: "Plants",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "SprayTime",
                table: "Plants",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "RotateTime",
                table: "Plants",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "RepotTime",
                table: "Plants",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "PruneTime",
                table: "Plants",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<DateOnly>(
                name: "PlantingDate",
                table: "Plants",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(DateOnly),
                oldType: "date");

            migrationBuilder.AlterColumn<string>(
                name: "Location",
                table: "Plants",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(50)");

            migrationBuilder.AlterColumn<int>(
                name: "FertilizeTime",
                table: "Plants",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AlterColumn<int>(
                name: "CleanTime",
                table: "Plants",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(2)");

            migrationBuilder.AddColumn<int>(
                name: "PlantId1",
                table: "Plants",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Plants_PlantId1",
                table: "Plants",
                column: "PlantId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Plants_Plants_PlantId1",
                table: "Plants",
                column: "PlantId1",
                principalTable: "Plants",
                principalColumn: "PlantId");
        }
    }
}
