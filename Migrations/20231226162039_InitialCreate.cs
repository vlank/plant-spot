﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WpfApp1.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Plants",
                columns: table => new
                {
                    PlantId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "varchar(20)", nullable: false),
                    Description = table.Column<string>(type: "varchar(100)", nullable: false),
                    Location = table.Column<string>(type: "TEXT", nullable: false),
                    PlantingDate = table.Column<DateOnly>(type: "TEXT", nullable: false),
                    WaterTime = table.Column<int>(type: "INTEGER", nullable: false),
                    SprayTime = table.Column<int>(type: "INTEGER", nullable: false),
                    RotateTime = table.Column<int>(type: "INTEGER", nullable: false),
                    PruneTime = table.Column<int>(type: "INTEGER", nullable: false),
                    FertilizeTime = table.Column<int>(type: "INTEGER", nullable: false),
                    RepotTime = table.Column<int>(type: "INTEGER", nullable: false),
                    CleanTime = table.Column<int>(type: "INTEGER", nullable: false),
                    PlantId1 = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plants", x => x.PlantId);
                    table.ForeignKey(
                        name: "FK_Plants_Plants_PlantId1",
                        column: x => x.PlantId1,
                        principalTable: "Plants",
                        principalColumn: "PlantId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Plants_PlantId1",
                table: "Plants",
                column: "PlantId1");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Plants");
        }
    }
}
