﻿using Microsoft.EntityFrameworkCore;
using WpfApp1.Interfaces;
using WpfApp1.Models;

namespace WpfApp1.Repositories;

public class CategoriesRepository : Repository<Category>, ICategoryRepository
{
    public CategoriesRepository(DbContext context) : base(context)
    {
        context.Database.EnsureCreated();
    }
}