﻿using Microsoft.EntityFrameworkCore;
using WpfApp1.Models;

namespace WpfApp1.Repositories;

public class PlantsRepository : Repository<Plant>, IPlantsRepository
{
    public PlantsRepository(DbContext context) : base(context)
    {
        context.Database.EnsureCreated();
    }

    public IEnumerable<Plant> GetOrderedPlants(int count)
    {
        return PlantsContext.Plants.OrderByDescending(p => p.Name).AsEnumerable();
    }

    public IEnumerable<Plant> GetPlantsByCategory(string category)
    {
        return PlantsContext.Plants.Where(p => p.Category.Name == category).AsEnumerable();
    }

    public PlantsContext PlantsContext
    {
        get {return Context as PlantsContext;}
    }
}