﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using WpfApp1.Interfaces;
using WpfApp1.Models;

namespace WpfApp1.Services;

public class PlantService
{
    private IUnitOfWork _unitOfWork;
    private readonly ObservableCollection<Plant> _plants = new();
    private readonly ObservableCollection<Category> _categories = new();

    public PlantService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public ObservableCollection<Plant> GetAll()
    {
        return _plants;
    }

    public ObservableCollection<Category> GetCategories()
    {
        return _categories;
    }

    public IEnumerable<Plant> GetByCategory(string category)
    {
        return _unitOfWork.Plants.GetPlantsByCategory(category);
    }

    public void LoadDatabase()
    {
        _plants.Clear();
        _categories.Clear();
        var plants = _unitOfWork.Plants.GetAll();
        foreach (var plant in plants)
        {
            _plants.Add(plant);
        }

        var categories = _unitOfWork.Categories.GetAll();
        foreach (var category in categories)
        {
            _categories.Add(category);
        }
    }

    public void RestartDatabase()
    {
        _unitOfWork.Plants.RemoveRange(GetAll());
        _unitOfWork.Save();
        LoadDatabase();
    }

    public void AddPlant(Plant plant)
    {
        _unitOfWork.Plants.Add(plant);
        _unitOfWork.Save();
    }

    public void EditPlant(PlantDto plant, Category category)
    {
        try
        {
            var oldPlant = _unitOfWork.Plants.GetById(plant.Id);
            if (oldPlant != null)
            {
                oldPlant.Name = plant.Name;
                oldPlant.Description = plant.Description;
                oldPlant.Location = plant.Location;
                oldPlant.Category = category;
                oldPlant.PlantingDate = plant.PlantingDate.HasValue
                    ? plant.PlantingDate
                    : null;
                oldPlant.WaterTime = plant.WaterTime;
                oldPlant.SprayTime = plant.SprayTime;
                oldPlant.RotateTime = plant.RotateTime;
                oldPlant.PruneTime = plant.PruneTime;
                oldPlant.FertilizeTime = plant.FertilizeTime;
                oldPlant.RepotTime = plant.RepotTime;
                oldPlant.ImageSource = plant.ImageSource;
                _unitOfWork.Plants.Update(oldPlant);
                _unitOfWork.Save();
            }
        }
        catch (Exception e)
        {
            Debug.Write(e);
        }
    }
}