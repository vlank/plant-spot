﻿![Header](/Docs/PlantSpot.png)

## Funcionamiento

Esta aplicación solicita y guarda información sobre tus plantas en
el almacenamiento local de tu computadora, para de esta forma, prescindir
del pago de una base de datos online y solicitar el cobro por el uso
de la misma.

## Vista previa
<img alt="vista general" src="Docs/preview.png" width="250" height="150"/>
<img alt="previsualización de planta" src="Docs/preview2.png" width="250" height="150"/>
<img alt="vista de edición" src="Docs/preview3.png" width="250" height="150"/>
<img alt="vista de adición" src="Docs/preview4.png" width="250" height="150"/>


## Instalación y uso
### Instalación

Puede descargar el programa
en [este enlace directo](https://gitlab.com/vlank/plant-spot/-/archive/750bce38b5d763ad4c48cba699a89365cdced6c4/plant-spot-750bce38b5d763ad4c48cba699a89365cdced6c4.zip?path=Releases)
o en las [releases](https://gitlab.com/vlank/plant-spot/-/releases) del repositorio.
El mismo es un archivo .zip que debe extraerse obteniendo como resultado
un ejecutable `.exe` y archivos de configuración ubicados en una carpeta llamada
`Releases` (_puede mover los archivos a cualquier otra carpeta_).
En la primer ejecución se generará un archivo llamado `plantings.db`
que será utilizado como base de datos y un directorio `Assets\Images` donde se
guardarán las imágenes que seleccione.

De momento, no se planea agregar un instalador y funcionará de manera portable, es decir,
los contenidos se ubicarán en una carpeta que puede mover y borrar en cualquier momento.

### Uso

Para utilizar el programa debe agregar información sobre las plantas seleccionándo
el icono
<img alt="add" src="Docs/add.png" width="20" height="20"/>
en la esquina superior derecha e indicar los datos
requeridos y guardarlos cuando haya completado los campos. Luego puede regresar,
seleccionado <img alt="back" src="Docs/back.png" width="20" height="20"/>, y verá una breve descripción de
la planta, la cual
puede seleccionar para obtener la información detallada. Asimismo, puede seleccionar
<img alt="edit" src="Docs/edit.png" width="20" height="20"/>, que le otorgará acceso a la modificación de
los datos en caso
de algún error en la carga.
Por último, puede reiniciar los datos de la aplicación, seleccionando
<img alt="restart" src="Docs/restart.png" width="20" height="20"/>, ubicado en la esquina superior izquierda
de la pantalla inicial.

## Información técnica

[Disponible aquí](Docs/TechDetails.md)
