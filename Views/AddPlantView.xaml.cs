﻿using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfApp1.Views;

public partial class AddPlantView : UserControl
{
    public AddPlantView()
    {
        InitializeComponent();
    }

    private void CheckOnlyNumbersInput(object sender, TextCompositionEventArgs e)
    {
        Regex regex = new Regex("[^0-9]+");
        e.Handled = regex.IsMatch(e.Text);
    }
}