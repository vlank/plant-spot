﻿using System.Collections.ObjectModel;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using WpfApp1.Helpers;
using WpfApp1.Models;
using WpfApp1.Services;

namespace WpfApp1.ViewModels;

public record PlantSender(Plant Plant);

public partial class HomeViewModel : ViewModelBase
{
    private PlantService _plantService;

    public HomeViewModel()
    {
    }

    public HomeViewModel(PlantService plantService)
    {
        _plantService = plantService;
        LoadPlants();
        LoadCategories();
    }

    private void LoadPlants()
    {
        _plantService.LoadDatabase();
        Plants = _plantService.GetAll();
    }

    private void LoadCategories()
    {
        _plantService.LoadDatabase();
        Categories = _plantService.GetCategories();
    }

    #region ObservableProperties

    [ObservableProperty] private ObservableCollection<Plant> _plants = new();
    [ObservableProperty] private ObservableCollection<Category> _categories = new();

    #endregion

    #region RelayCommands

    [RelayCommand]
    private void GetByCategory(string category)
    {
        Plants.Clear();
        foreach (var plant in _plantService.GetByCategory(category))
        {
            Plants.Add(plant);
        }
    }

    [RelayCommand]
    private void GetAll()
    {
        LoadPlants();
    }

    [RelayCommand]
    private void PreviewPlant(Plant plant)
    {
        Messenger.Send(new PlantSender(plant));
        Messenger.Send(new GoToPlantView());
    }

    [RelayCommand]
    private void RouteToAddView()
    {
        Messenger.Send(new GoToAddView());
    }

    [RelayCommand]
    private void ResetDatabase()
    {
        var confirmation = MessageBox.Show("¿Deseas borrar todas las plantas?",
            "", MessageBoxButton.YesNo);
        switch (confirmation)
        {
            case MessageBoxResult.Yes:
            {
                try
                {
                    _plantService.RestartDatabase();
                    ImageHelper.DeleteAllImages();
                    MessageBox.Show("Se han borrado todos los datos");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                break;
            }
            case MessageBoxResult.No:
            {
                MessageBox.Show("No se han realizado cambios");
                break;
            }
            case MessageBoxResult.Cancel:
            {
                break;
            }
        }
    }
    #endregion
}