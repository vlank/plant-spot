﻿using System.Diagnostics;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Input;
using WpfApp1.Interfaces;
using WpfApp1.Models;
using WpfApp1.Services;

namespace WpfApp1.ViewModels;

public record GoToHomeView();

public record GoToAddView();

public record GoToPlantView();
public record GoToEditView();

public partial class MainViewModel : ViewModelBase
{
    private readonly PlantsContext _plantsContext = new();

    private ViewModelBase _addPlantView;
    [ObservableProperty] private ViewModelBase _current;
    private ViewModelBase _editPlantView;
    private ViewModelBase _generalView;

    private PlantService _plantService;
    private ViewModelBase _plantView;


    public MainViewModel()
    {
        IUnitOfWork unitOfWork = new UnitOfWork(_plantsContext);
        // Services
        _plantService = new PlantService(unitOfWork);

        CreateViewModels();
        Current = _generalView!;

        RegisterRoutes();
    }

    private void CreateViewModels()
    {
        _generalView = new HomeViewModel(_plantService);
        _addPlantView = new AddPlantViewModel(_plantService);
        _plantView = new PlantViewModel();
        _editPlantView = new EditPlantViewModel(_plantService);
    }

    private void RegisterRoutes()
    {
        Messenger.Register<GoToHomeView>(this, (recipient, message) => Current = _generalView);
        Messenger.Register<GoToAddView>(this, (recipient, message) => Current = _addPlantView);
        Messenger.Register<GoToPlantView>(this, (recipient, message) => Current = _plantView);
        Messenger.Register<GoToEditView>(this, (recipient, message) => Current = _editPlantView);
    }
}