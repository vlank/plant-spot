﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using WpfApp1.Models;

namespace WpfApp1.ViewModels;
public record PlantDtoSender(PlantDto PlantDto);
public partial class PlantViewModel : ViewModelBase
{
    [ObservableProperty] private Plant _plant;

    [RelayCommand]
    private void RouteToHomeView()
    {
        Messenger.Send(new GoToHomeView());
    }

    [RelayCommand]
    private void RouteToEditView(Plant plant)
    {
        Messenger.Send(new PlantDtoSender( new PlantDto(
            id: plant.Id,
            name: plant.Name,
            category: plant.Category,
            description: plant.Description,
            location: plant.Location,
            imageSource: plant.ImageSource,
            plantingDate: plant.PlantingDate,
            waterTime: plant.WaterTime,
            sprayTime: plant.SprayTime,
            rotateTime: plant.RotateTime,
            pruneTime: plant.PruneTime,
            fertilizeTime: plant.FertilizeTime,
            repotTime: plant.RepotTime
        )));
        Messenger.Send(new GoToEditView());
    }

    public PlantViewModel()
    {
        Messenger.Register<PlantSender>(this,(recipient, message) => Plant = message.Plant);
    }

    
}