﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Microsoft.Win32;
using WpfApp1.Helpers;
using WpfApp1.Models;
using WpfApp1.Services;

namespace WpfApp1.ViewModels;

public partial class EditPlantViewModel : ViewModelBase
{
    private readonly PlantService _plantService;


    public EditPlantViewModel(PlantService plantService)
    {
        _plantService = plantService;
        Messenger.Register<PlantDtoSender>(this, HandlePlantMessage);
        LoadCategories();
    }

    private void HandlePlantMessage(object recipient, PlantDtoSender message)
    {
        Plant = message.PlantDto;
        if (Plant != null)
        {
            SelectedCategory = Categories.First(c => c.Id == Plant.Category.Id);
        }
    }


    private void LoadCategories()
    {
        _plantService.LoadDatabase();
        Categories = _plantService.GetCategories();
    }

    private void CopyImage(string imagePath, string directory)
    {
        try
        {
            string path = Path.Combine(directory, Path.GetFileName(imagePath));
            File.Copy(imagePath, path, true);
            if (Plant != null) Plant.ImageSource = path;
            MessageBox.Show("Imagen guardada");
        }
        catch (Exception e)
        {
            MessageBox.Show(e.Message);
        }
    }

    private string EnsureCreatedImagesPath()
    {
        var directory = Environment.CurrentDirectory + @"\Assets\Images";
        if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
        return directory;
    }

    #region ObservableProperties

    [ObservableProperty] private ObservableCollection<Category> _categories = new();
    [ObservableProperty] private PlantDto? _plant;
    [ObservableProperty] private Category _selectedCategory;

    #endregion

    #region RelayCommands

    [RelayCommand]
    private void RouteToHomeView()
    {
        _plantService.LoadDatabase();
        Messenger.Send(new GoToPlantView());
    }

    [RelayCommand]
    private void UpdatePlant()
    {
        if (Plant != null)
        {
            _plantService.EditPlant(Plant, SelectedCategory);
        }

        MessageBox.Show("Modificado correctamente");
    }

    [RelayCommand]
    private void LoadImage()
    {
        if (Plant != null) Plant.ImageSource = ImageHelper.LoadImage();
    }

    #endregion
}