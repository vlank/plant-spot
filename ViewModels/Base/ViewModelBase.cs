﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;

namespace WpfApp1.ViewModels;

public class ViewModelBase : ObservableValidator
{
    protected ViewModelBase()
        : this(WeakReferenceMessenger.Default)
    {
    }

    protected ViewModelBase(IMessenger messenger)
    {
        Messenger = messenger;
    }

    protected IMessenger Messenger { get; }
}