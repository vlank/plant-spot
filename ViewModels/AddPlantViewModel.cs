﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using WpfApp1.Helpers;
using WpfApp1.Models;
using WpfApp1.Services;

namespace WpfApp1.ViewModels;

public partial class AddPlantViewModel : ViewModelBase
{
    private readonly PlantService _plantService;

    public AddPlantViewModel()
    {
    }

    public AddPlantViewModel(PlantService plantService)
    {
        _plantService = plantService;
        LoadCategories();
    }

    private void CleanData()
    {
        Name = Description = Location = null;
        SelectedCategory = null;
        ImageSource = null;
        WaterTime = RotateTime = PruneTime = FertilizeTime = SprayTime = RepotTime = 0;
        PlantingDate = null;
    }

    private void LoadCategories()
    {
        _plantService.LoadDatabase();
        Categories = _plantService.GetCategories();
    }

    #region ObservableProperties

    [ObservableProperty] private ObservableCollection<Category> _categories = new();
    [ObservableProperty] private Category? _selectedCategory;

    [ObservableProperty]
    [NotifyDataErrorInfo]
    [MaxLength(29, ErrorMessage = "Ha alcanzado el límite de 30 caracteres")]
    [Required(ErrorMessage = "El nombre es obligatorio")]
    private string? _name;

    [ObservableProperty]
    [NotifyDataErrorInfo]
    [MaxLength(99, ErrorMessage = "Ha alcanzado el límite de 100 caracteres")]
    private string? _description;

    [ObservableProperty] [NotifyDataErrorInfo] [MaxLength(39, ErrorMessage = "Ha alcanzado el límite de 40 caracteres")]
    private string? _location;

    [ObservableProperty] private DateTime? _plantingDate;

    [ObservableProperty] private int _waterTime;
    [ObservableProperty] private int _sprayTime;
    [ObservableProperty] private int _rotateTime;
    [ObservableProperty] private int _pruneTime;
    [ObservableProperty] private int _fertilizeTime;
    [ObservableProperty] private int _repotTime;
    [ObservableProperty] private string? _imageSource;

    #endregion

    #region RelayCommands

    [RelayCommand]
    private void AddPlant()
    {
        try
        {
            if (Location != null && SelectedCategory != null)
            {
                _plantService.AddPlant(new Plant()
                {
                    Name = Name,
                    Description = Description,
                    Location = Location,
                    Category = SelectedCategory,
                    PlantingDate = PlantingDate.HasValue
                        ? DateOnly.Parse(PlantingDate.Value.Date.ToShortDateString())
                        : null,
                    WaterTime = WaterTime,
                    SprayTime = SprayTime,
                    RotateTime = RotateTime,
                    PruneTime = PruneTime,
                    FertilizeTime = FertilizeTime,
                    RepotTime = RepotTime,
                    ImageSource = ImageSource
                });
                MessageBox.Show("Guardado correctamente");
            }
            else
            {
                MessageBox.Show("Complete los campos vacíos");
            }
        }
        catch (Exception e)
        {
            MessageBox.Show(e.Message);
        }
        finally
        {
            CleanData();
        }
    }


    [RelayCommand]
    private void LoadImage()
    {
        ImageSource = ImageHelper.LoadImage(); ;
    }

    [RelayCommand]
    private void CleanFields() => CleanData();

    [RelayCommand]
    private void RouteToHomeView()
    {
        CleanData();
        _plantService.LoadDatabase();
        Messenger.Send(new GoToHomeView());
    }

    #endregion
}